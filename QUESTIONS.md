## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?

2. Quais foram os últimos dois framework/CMS que você trabalhou?

3. Descreva os principais pontos positivos do seu framework favorito.

4. Descreva os principais pontos negativos do seu framework favorito.

5. O que é código de qualidade para você.

## Conhecimento Linux

1. O que é software livre?

2. Qual o seu sistema operacional favorito?

3. Já trabalhou com Linux ou outro Unix-like?

4. O que é SSH?

5. Quais as principais diferenças entre sistemas *nix e o Windows?

## Conhecimento de desenvolvimento

1. O que é GIT?

2. Descreva um workflow simples de trabalho utilizando GIT.

3. O que é PHP Data Objects?

4. O que é Database Abstract Layer?

5. Você sabe o que é Object Relational Mapping? Se sim, explique.

6. Como você avalia seu grau de conhecimento em Orientação a objeto?

7. O que é Dependency Injection?

8. O significa a sigla S.O.L.I.D?

9. Qual a finalidade do framework PHPUnit?

10. Explique o que é MVC.